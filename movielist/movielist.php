<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Vincent's Movie List</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css"/>
</head>
<body>
<header><?php include '../Includes/header.php'?></header>
<nav> <?php include '../Includes/nav.php'?> </nav>
<main>
    <h3>Movie List</h3>

    <table align="center" border="1" width="90%">
        <tr>
            <th>Key</th>
            <th>Movie Title</th>
            <th>Movie Rating</th>
        </tr>

        <?php
        include "../Includes/dbconnect.php";
        try{
            $db = new PDO($dsn,$username,$password,$options);
            $sql = $db->prepare("Select * from movielist1");
            $sql->execute();

        }catch(PDOException $e){
            echo $e->getMessage();
        }
        ?>
    </table>
    <br /><br />
    <a href="movieadd.php">Add New Movie</a>
</main>
<footer> <?php include '../Includes/footer.php'?></footer>
</body>
</html>