<?php
    $msg="";
    if(isset($_POST["btnSubmit"]))
    {
        if (!empty($_POST["txtTitle"]))
        {
            if (!empty($_POST["txtRating"]))
            {
                $title = $_POST["txtTitle"];
                $rating = $_POST["txtRating"];

                //Database stuff
                include "../Includes/dbconnect.php";

                try {
                    $db = new PDO($dsn, $username, $password, $options);
                    $sql = $db->prepare("insert into movielist1 (movieTitle, movieRating) VALUE(:Title,:Rating)");
                    $sql->bindValue(":Title", $title);
                    $sql->bindValue(":Rating", $rating);
                    $sql->execute();
                    header("location:movielist.php");
                } catch (PDOException $e) {
                    echo $e->getMessage();
                }
            } else {
                $msg = "No Rating";
            }
        } else {
            $msg = "No Title";
        }
    }?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Add Movie</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css"/>
</head>
<body>
<header><?php include '../Includes/header.php'?></header>
<nav> <?php include '../Includes/nav.php'?> </nav>
<main>

    <h3><?=$msg?></h3>

    <form method="post">
        <TABLE align="center"border="1" width="80%">
            <tr height="60px">
                <th colspan="2"><h3>Add New Movie</h3></th>
            </tr>
            <tr height="60px">
                <th>Movie Name</th>
                <td><input id="txtTitle" name="txtTitle" type="text"></td>
            </tr>
            <tr height="60px">
                <th>Movie Rating</th>
                <td><input id="txtRating" name="txtRating" type="text"></td>
            </tr>
            <tr height="60px">
                <td colspan="2">
                    <input name="btnSubmit" id="btnSubmit" type="submit" value="Add New Movie">
                </td>
            </tr>
        </TABLE>
    </form>
</main>
<footer> <?php include '../Includes/footer.php'?></footer>
</body>
</html>