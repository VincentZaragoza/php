<?php
    if(empty($_GET["id"]))
    {
        header("location:movielist.php");
    }

    if(isset($_GET["id"])) {
        $id = $_GET["id"];

        //Database stuff
        include "../Includes/dbconnect.php";
        try {
            $db = new PDO($dsn, $username, $password, $options);
            $sql = $db->prepare("delete from movielist1 where movieID = :id");
            $sql->bindValue(":id", $id);
            $sql->execute();
            header("location:movielist.php");
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }?>