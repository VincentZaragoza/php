<?php
    if(empty($_GET["id"]))
    {
        header("location:movielist.php");
    }

    if(!empty($_POST["txtTitle"]))
    {
        if(!empty($_POST["txtRating"]))
        {
            $title = $_POST["txtTitle"];
            $rating = $_POST["txtRating"];
            $id = $_POST["txtID"];
            include "../Includes/dbconnect.php";
            try {
                $db = new PDO($dsn, $username, $password, $options);
                $sql = $db->prepare("update movielist1 set movieTitle = :Title, movieRating = :Rating where movieID = :id");
                $sql->bindValue(":Title", $title);
                $sql->bindValue(":Rating", $rating);
                $sql->bindValue(":id", $id);
                $sql->execute();

                header("location:movielist.php");
            } catch (PDOException $e) {
                echo $e->getMessage();
            }
        }
    }

    if(isset($_GET["id"]))
    {
        $id=$_GET["id"];

        //Database stuff
        include "../Includes/dbconnect.php";
        try {
            $db = new PDO($dsn, $username, $password, $options);
            $sql = $db->prepare("select * from movielist1 where movieID = :id");
            $sql->bindValue(":id", $id);
            $sql->execute();
            $row = $sql->fetch();

            $title = $row["movieTitle"];
            $rating= $row["movieRating"];
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

    }?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Add Movie</title>
    <script>
        function DeleteMovie(title, id)
        {
            if (confirm("Do you want to delete " + title + "?"))
            {
                document.location.href = "moviedelete.php?id=" + id;
            }
        }
    </script>
    <link rel="stylesheet" type="text/css" href="../css/base.css"/>
</head>
<body>
<header><?php include '../Includes/header.php'?></header>
<nav> <?php include '../Includes/nav.php'?> </nav>
<main>

    <h3><?=$msg?></h3>

    <form method="post">
        <table align="center" border="1" width="80%">
            <tr>
                <th colspan="2"><h3>Update Movie</h3></th>
            </tr>
            <tr>
                <th>Movie Name</th>
                <td><input id="txtTitle" value="<?=$title?>" name="txtTitle" type="text" ></td>
            </tr>
            <tr >
                <th>Movie Rating</th>
                <td><input id="txtRating" value="<?=$rating?>" name="txtRating" type="text"></td>
            </tr>
            <tr >
                <td colspan="2">
                    <input name="btnSubmit" id="btnSubmit" type="submit" value="Update Movie">
                    <input type="button" value="Delete Movie" onclick="DeleteMovie('<?=$title?>','<?=$id?>')">
                </td>
            </tr>
        </table>
            <input name="txtID" id="txtID" type="hidden" value="<?=$id?>">
    </form>
</main>
<footer> <?php include '../Includes/footer.php'?></footer>
</body>
</html>