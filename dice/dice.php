<?php
$dice1 = mt_rand(1,6);
$dice2 = mt_rand(1,6);
$dice3 = mt_rand(1,6);
$dice4 = mt_rand(1,6);
$dice5 = mt_rand(1,6);

$playerScore = $dice1 + $dice2;
$cpuScore = $dice3 + $dice4 + $dice5;

if($playerScore > $cpuScore){
    $winner = "You won";
}else if($playerScore == $cpuScore){
    $winner = "Tie";
}else {
    $winner = "Computer won";
}



?>


<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Vincent's Dice Game</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css"/>
    <link rel="stylesheet" type="text/css" href="../css/dice.css"/>
</head>
<body>
<header><?php include '../Includes/header.php'?></header>
<nav> <?php include '../Includes/nav.php'?> </nav>
<main>
    <h3>Your score: <?=$playerScore?></h3>

    <div class="row">
        <div class="column">
            <img src="/images/dice/dice_<?=$dice1 ?>.png" id="dice1" alt="dice1"/>
        </div>
        <div class="column">
            <img src="/images/dice/dice_<?=$dice2 ?>.png" id="dice2" alt="dice2"/>
        </div>
    </div>

    <h3>Computer score: <?=$cpuScore?></h3>

    <div class="row">
        <div class="column">
            <img src="/images/dice/dice_<?=$dice3 ?>.png" id="dice3" alt="dice3"/>
        </div>
        <div class="column">
            <img src="/images/dice/dice_<?=$dice4 ?>.png" id="dice4" alt="dice4"/>
        </div>
        <div class="column">
            <img src="/images/dice/dice_<?=$dice5 ?>.png" id="dice3" alt="dice5"/>
        </div>
    </div>

    <h3>Result: <?php echo $winner?></h3>
</main>
<footer> <?php include '../Includes/footer.php'?></footer>
</body>
</html>
