<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Vincent's Homepage</title>
    <link rel="stylesheet" type="text/css" href="css/base.css"/>
</head>
<body>
    <header><?php include 'Includes/header.php'?></header>
    <nav> <?php include 'Includes/nav.php'?> </nav>
    <main>
        <img src="images/bear.jpg" alt="Vincent Zargoza"/>
        <p>What would you do if you were walking through the woods when suddenly a big brown bear popped out of the water?
            Only instead of coming after you, he simply started to wave hello. Bears are far more human-like than you might think.
            According to Grizzle Bay statistics, bears are actually less likely to be a psycho killer than humans.
            While 1 out of every 16,000 people is born a murder, only 1 grizzly bear out of every 50,000 will kill a human.</p>
    </main>
    <footer> <?php include '../Includes/footer.php'?></footer>
</body>
</html>