<?php
    session_start();
    $responses = array();
    $responses [0] = "Ask again later";
    $responses [1] = "Yes";
    $responses [2] = "No";
    $responses [3] = "It appears to be so";
    $responses [4] = "Reply is hazy, please try again";
    $responses [5] = "Yes, definitely";
    $responses [6] = "What is it you really want to know";
    $responses [7] = "Outlook is good ";
    $responses [8] = "My sources say no";
    $responses [9] = "My sources say yes";
    $responses [10] = "Don't count on it ";
    $responses [11] = "Can not predict now";
    $responses [12] = "As I can see it, yes";
    $responses [13] = "Better not tell you now";
    $responses [14] = "Concentrate and ask again";

    if(isset($_POST["txtQuestion"])){
        $question = $_POST["txtQuestion"];
    }else{
        $question = "";
    }

    if(isset($_SESSION["PreQuest"])){
        $PreQuest = $_SESSION["PreQuest"];
    }else{
        $PreQuest = "";
    }

    if($question == ""){
        $answer = "Ask me a Question";
    }else if(substr($question,-1)!="?"){
        $answer = "Ask me with a question mark!!";
    }else if($PreQuest == $question){
        $answer = "Please Ask me a new question!!";
    }else{
        $num = mt_rand(0,14);
        $answer = $responses[$num];
        $_SESSION["PreQuest"] = $question;
    }


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Vincent's Magic 8 Ball</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css"/>
</head>
<body>
<header><?php include '../Includes/header.php'?></header>
<nav> <?php include '../Includes/nav.php'?> </nav>
<main>
    <h2>Magic 8 Ball</h2>
    <br />
    <marquee><?=$answer?></marquee>
    <br />
    <p>Ask a question<br/>
    <form method="post" action="magic8.php">
        <input type="text" id="txtQuestion" name="txtQuestion" value="<?=$question?>"/>
        <input type="submit" value="Ask the 8 Ball">
    </form>
</main>
<footer> <?php include '../Includes/footer.php'?></footer>
</body>
</html>
