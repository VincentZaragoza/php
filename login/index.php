<?php
session_start();

if(isset($_POST["txtEmail"])){
    if(isset($_POST["txtPassword"])){
        $Email = $_POST["txtEmail"];
        $pwd = $_POST["txtPassword"];
        $errmsg = "";

        include "../Includes/dbconnect.php";
        try{
            $db = new PDO($dsn,$username,$password,$options);
            $sql = $db->prepare("Select memberPassword ,memberID,memberKey,RoleID from memberLogin where memberEmail = :Email");
            $sql->bindValue(":Email",$Email);
            $sql->execute();
            $row = $sql->fetch();

            if($row!=null){
                $hashedPassword = md5($pwd.$row["memberKey"]);

                if($hashedPassword == $row["memberPassword"])
                {
                    $_SESSION["UID"] = $row["memberID"];
                    $_SESSION["Role"] =$row["RoleID"];
                    if($row["RoleID"] ==1){
                        header("Location:admin.php");
                    }else{
                        header("Location:member.php");
                    }
                }else{
                    $errmsg="Wrong Password";
                     }
            }else{
                $errmsg="Wrong Username ";
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
}?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css"/>
</head>
<body>
<header><?php include '../Includes/header.php'?></header>
<nav> <?php include '../Includes/nav.php'?> </nav>
<main>
    <form method="post">
        <h3><?=$errmsg?></h3>
        <table align="center" border="1" width="80%">
            <tr>
                <th colspan="2"><h3>User Login</h3></th>
            </tr>
            <tr>
                <th>Email</th>
                <td><input id="txtUserName" name="txtEmail" type="text" ></td>
            </tr>
            <tr>
                <th>Password</th>
                <td><input id="txtPassword" name="txtPassword" type="password"></td>
            </tr>
            <tr >
                <td colspan="2">
                    <input name="btnSubmit" id="btnSubmit" type="submit" value="Login">
                </td>
            </tr>
        </table>
    </form>
</main>
<footer> <?php include '../Includes/footer.php'?></footer>
</body>
</html>