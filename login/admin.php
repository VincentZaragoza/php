<?php
session_start();
$errmsg ="";
$key =  sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));

if($_SESSION["Role"] != 1) {
    header("Location:index.php");
}
if(isset($_POST["btnSubmit"])){
   if(empty($_POST["txtPassword"])){
        $errmsg = "Password is required.";
    }else{
        $Password=$_POST["txtPassword"];
    }
    if($Password != $_POST["txtPassword2"]){
        $errmsg = "Passwords do not match.";
    }

    if(empty($_POST["txtEmail"])){
        $errmsg = "Email is required.";
    }else{
        $Email=$_POST["txtEmail"];
    }
    if(empty($_POST["txtFName"])){
        $errmsg = "Name is required.";
    }else{
        $FName=$_POST["txtFName"];
    }

    if($errmsg==""){
        //Database stuff
        include "../Includes/dbconnect.php";

        try {
            $db = new PDO($dsn, $username, $password, $options);
            $sql = $db->prepare("insert into memberLogin (memberName, memberEmail, memberPassword, RoleID, memberKey)  VALUE(:Name, :Email, :Password, :RID, :Key)");
            $sql->bindValue(":Name", $FName);
            $sql->bindValue(":Email", $Email);
            $sql->bindValue(":Password", md5($Password . $key));
            $sql->bindValue(":RID", $RoleID);
            $sql->bindValue(":Key", $key);
            $sql->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        $FName="";
        $Email="";
        $Password="";
        $RoleValue="";
        $errmsg = "Member Added to Database";
    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Admin Page</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css"/>
</head>
<body>
<header><?php include '../Includes/header.php'?></header>
<nav> <?php include '../Includes/nav.php'?> </nav>
<main>
    <h1>Admin Page</h1>
    <h3><?=$errmsg?></h3>
    <form method="post">
        <table align="center" border="1" width="80%">
            <tr>
                <th colspan="2"><h3>Add New Member</h3></th>
            </tr>
            <tr>
                <th>Full Name</th>
                <td><input id="txtFName" name="txtFName" value="<?=$FName?>" type="text" ></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><input id="txtEmail" name="txtEmail" value="<?=$Email?>" type="text"  ></td>
            </tr>
            <tr>
                <th>Password</th>
                <td><input id="txtPassword" name="txtPassword" type="password" ></td>
            </tr>
            <tr>
                <th>Retype Password</th>
                <td><input id="txtPassword2" name="txtPassword2" type="password"></td>
            </tr>
            <tr>


                <th>Role</th>
                <td>
                    <select name="rolename">
                    <?php
                    include "../Includes/dbconnect.php";
                    $db = new PDO($dsn, $username, $password, $options);
                    $sql = $db->prepare("select RoleValue,RoleID from role"); // Run your query
                    $sql->execute();


                    while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                        $RoleValue = $row['RoleValue'];
                        echo "<option value='$RoleID'>$RoleValue</option>";
                    }


                    ?>
                    </select>
                </td>
            </tr>

            <tr >
                <td colspan="2">
                    <input name="btnSubmit" id="btnSubmit" type="submit" value="Add New Member">
                </td>
            </tr>
        </table>
    </form>
</main>
<footer> <?php include '../Includes/footer.php'?></footer>
</body>
</html>