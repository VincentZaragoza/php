<?php
session_start();
if(!isset($_SESSION["UID"]))
    header("Location:index.php")
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Member Page</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css"/>
</head>
<body>
<header><?php include '../Includes/header.php'?></header>
<nav> <?php include '../Includes/nav.php'?> </nav>
<main>
    <h1>Member Page</h1>
</main>
<footer> <?php include '../Includes/footer.php'?></footer>
</body>
</html>