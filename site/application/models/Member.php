<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Model{

    public function User_Login($Email,$pwd){

        $this->load->database();
        $this->load->library("session");
        try{
            $db = new PDO ($this->db->dsn, $this->db->username,$this->db->password,$this->db->options);
            $sql = $db->prepare("Select memberPassword ,memberID,memberKey,RoleID from memberLogin where memberEmail = :Email");
            $sql->bindValue(":Email",$Email);
            $sql->execute();
            $row = $sql->fetch();

            if($row!=null){
                $hashedPassword = md5($pwd.$row["memberKey"]);

                if($hashedPassword == $row["memberPassword"]){
                    $this->session->set_userdata(array('UID'=>$row['memberID']));
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }catch(PDOException $e) {
            return false;
        }
    }
}
