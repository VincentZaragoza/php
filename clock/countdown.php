<?php
/*
 * This is a countdown
 * Burning Man 8-25-2020
 */

$secPerMin = 60;
$secPerHR = 60* $secPerMin;
$secPerDay = 24 * $secPerHR;
$secPerMonth = 31 * $secPerDay;
$secPerYear = 12 * $secPerMonth;

//Current Time
$now = time();

//EOS time
$eos = mktime(21,0,0,5,8,2019);

//Number of seconds between now then
$seconds = $eos - $now;

$years = floor($seconds/$secPerYear);
$seconds = $seconds - ($years * $secPerYear);

$Month = floor($seconds / $secPerMonth);
$seconds = $seconds - ($Month * $secPerMonth);


$Days = floor($seconds / $secPerDay);
$seconds = $seconds - ($Days * $secPerDay);

$Hours = floor($seconds /$secPerHR);
$seconds = $seconds - ($Hours * $secPerHR);

$Minutes = floor($seconds / $secPerMin);
$seconds = $seconds -($Minutes * $secPerMin);


?>

<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Vincent's Countdown</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css"/>
</head>
<body>
<header><?php include '../Includes/header.php'?></header>
<nav> <?php include '../Includes/nav.php'?> </nav>
<main>
    <h3>End of the semester Countdown</h3>
    <p>Months: <?=$Month?> Days: <?=$Days?> Hours: <?=$Hours?> Minutes: <?=$Minutes?> Seconds: <?=$seconds?></p>
    <img src="../images/friends.jpg" alt="Friends"/>
</main>
<footer> <?php include '../Includes/footer.php'?></footer>
</body>
</html>
