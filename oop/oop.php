<?php
class Car{
    public $color;
    public $make;
    public $model;
    public $year;
    public $status;

    function _construct(){
        $this->$this->status = "Stopped";
    }

    function Forward(){
        $this->status = "Forward";
    }
    function Reverse(){
        $this->status = "Backward";
    }
    function Stopped(){
        $this->status = "Stopped";
    }
}

$myCar = new Car();
$myCar->color = 'Yellow';
$myCar->make = 'Jeep';
$myCar->model = 'Wrangler';
$myCar->year = '2010';

echo "My " . $myCar->make . " is a " . $myCar->year . "<br/><br/><br/>";

echo $myCar->Forward();
$myCar->Reverse();
echo $myCar->status;

