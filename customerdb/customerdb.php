<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Vincent's Customer Database</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css"/>
</head>
<body>
<header><?php include '../Includes/header.php'?></header>
<nav> <?php include '../Includes/nav.php'?> </nav>
<main>
    <h3>Customer Listing</h3>
    <table align="center" border="1" width="90%">
        <tr>
            <th>Customer ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Zip</th>
            <th>Phone</th>
            <th>Email</th>
        </tr>
        <?php

        include '../Includes/dbconnect.php';

        try
        {
            $db = new PDO($dsn,$username,$password,$options);
            $sql = $db->prepare("select * from customers");
            $sql->execute();
            $row = $sql->fetch();

            while ($row!=null)
            {
                $id = $row["custID"];
                $custFirstName = $row["custFirstName"];
                $custLastName = $row["custLastName"];
                $custAddress = $row["custAddress"];
                $custCity = $row["custCity"];
                $custState = $row["custState"];
                $custZip = $row["custZip"];
                $custPhone = $row["custPhone"];
                $custEmail = $row["custEmail"];
                $custPassword = $row["custPassword"];

                echo "
                    <tr>
                        <td><a href='updateaccount.php?id=$id'>$id</a></td>
                        <td><a href='updateaccount.php?id=$id'>$custFirstName</a></td>
                        <td><a href='updateaccount.php?id=$id'>$custLastName</a></td>
                        <td><a href='updateaccount.php?id=$id'>$custAddress</a></td>
                        <td><a href='updateaccount.php?id=$id'>$custCity</a></td>
                        <td><a href='updateaccount.php?id=$id'>$custState</a></td>
                        <td><a href='updateaccount.php?id=$id'>$custZip</a></td>
                        <td><a href='updateaccount.php?id=$id'>$custPhone</a></td>
                        <td><a href='updateaccount.php?id=$id'>$custEmail</a></td>
                    </tr>";
                $row=$sql->fetch();
            }
        }catch (PDOException $e)
        {
            echo $e->getMessage();
        }?>
    </table></br>
    <a href="createaccount.php">Create Account</a>
</main>
<footer> <?php include '../Includes/footer.php'?></footer>
</body>
</html>