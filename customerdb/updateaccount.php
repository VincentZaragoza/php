<?php

if (!empty($_POST["txtFName"]))
{
    if (!empty($_POST["txtLName"]))
    {
        if (!empty($_POST["txtPhoneNum"]))
        {
            if (!empty($_POST["txtEmail"]))
            {
                if (!empty($_POST["txtAddress"]))
                {
                    if (!empty($_POST["txtCity"]))
                    {
                        if (!empty($_POST["txtZipCode"]))
                        {
                            if (!empty($_POST["txtState"]))
                            {
                                if( $_POST["txtState"]!= 'NA')
                                {
                                    if(!empty($_POST["txtPassword"]))
                                    {
                                        if(!empty($_POST["txtConfirmPassword"]))
                                        {
                                            if($state != 'NA')
                                            {
                                                if($_POST["txtPassword"] == $_POST["txtConfirmPassword"])
                                                {
                                                    $id = $_POST["txtID"];
                                                    $custFirstName = $_POST["txtFName"];
                                                    $custLastName= $_POST["txtLName"];
                                                    $custPhone= $_POST["txtPhoneNum"];
                                                    $custEmail = $_POST["txtEmail"];
                                                    $custAddress = $_POST["txtAddress"];
                                                    $custState = $_POST["txtState"];
                                                    $custCity = $_POST["txtCity"];
                                                    $custZip = $_POST["txtZipCode"];
                                                    $custPassword = $_POST["txtPassword"];

                                                    include "../Includes/dbconnect.php";
                                                    try{
                                                        $db = new PDO($dsn, $username, $password, $options);
                                                        $sql = $db->prepare("update customers set custFirstName=:FirstName, custLastName=:LastName,custAddress=:Address,
                                                        custCity=:City,custState=:State,custZip=:Zip,custPhone=:PhoneNumber,custEmail=:Email,custPassword=:Password where custID = :id");
                                                        $sql->bindValue(":id", $id);
                                                        $sql->bindValue(":FirstName", $custFirstName);
                                                        $sql->bindValue(":LastName", $custLastName);
                                                        $sql->bindValue(":Address", $custAddress);
                                                        $sql->bindValue(":City", $custCity);
                                                        $sql->bindValue(":State", $custState);
                                                        $sql->bindValue(":Zip", $custZip);
                                                        $sql->bindValue(":PhoneNumber", $custPhone);
                                                        $sql->bindValue(":Email", $custEmail);
                                                        $sql->bindValue(":Password", $custPassword);
                                                        $sql->execute();
                                                        header("location:customerdb.php");
                                                    }catch (PDOException $e)
                                                    {
                                                        echo $e->getMessage();
                                                    }
                                                }else{$msg ="Passwords Didn't Match";}
                                            }else {$msg = "No State";}
                                        }else {$msg = "Verify Password";}
                                    }else {$msg = "No Password";}
                                }else {$msg = "No State";}
                            }else{$msg = "No State";}
                        }else{$msg = "No Zip Code";}
                    }else{$msg = "No City";}
                }else{$msg = "No Address";}
            }else{$msg = "No Email";}
        }else{$msg = "No Phone Number";}
    }else{$msg = "No Last Name";}
}else{$msg = "No First Name";}

if(isset($_GET["id"]))
{
    $id=$_GET["id"];

    try{
        include "../Includes/dbconnect.php";
        $db = new PDO($dsn, $username, $password, $options);
        $sql = $db->prepare("select * from customers where custID = :id");
        $sql->bindValue(":id", $id);
        $sql->execute();
        $row = $sql->fetch();

        $custFirstName = $row["custFirstName"];
        $custLastName = $row["custLastName"];
        $custAddress = $row["custAddress"];
        $custCity = $row["custCity"];
        $custState = $row["custState"];
        $custZip = $row["custZip"];
        $custPhone = $row["custPhone"];
        $custEmail = $row["custEmail"];
        $custPassword = $row["custPassword"];

    }catch (PDOException $e)
    {
        echo $e->getMessage();
    }

}else{
    header("location:customerdb.php");
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Update Account</title>
    <script>
        function DeleteAccount(id)
        {
            if (confirm("Do you want to delete account?"))
            {
                document.location.href = "deleteaccount.php?id=" + id;
            }
        }
    </script>
    <link rel="stylesheet" type="text/css" href="../css/base.css"/>
    <link rel="stylesheet" type="text/css" href="../css/create_account.css"/>
</head>
<body>
<header><?php include '../Includes/header.php'?></header>
<nav> <?php include '../Includes/nav.php'?> </nav>
<main>


    <form method="post">
        <fieldset>
            <legend>Customer</legend>
            <label>First Name:</label> <input name="txtFName" type="text" value="<?=$custFirstName?>">
            <label>Last Name:</label> <input name="txtLName" type="text"  value="<?=$custLastName?>">
            <label>Phone Number:</label> <input name="txtPhoneNum"  type="tel"  value="<?=$custPhone?>" ">
            <label>Email:</label> <input name="txtEmail"  type="email"  value="<?=$custEmail?>">
        </fieldset>
        <fieldset>
            <legend>Address</legend>
            <label>Address:</label> <input name="txtAddress"  type="text"/><br>
            <label>City:</label> <input name="txtCity" type="text" value="<?=$custCity?>"><br>
            <label>Zip Code:</label> <input name="txtZipCode" type="text" value="<?=$custZip?>"><br>
            <label>State:</label> <select name="txtState" value="<?=$custState?>">
                <option value="NA">Select State</option>
                <option value="AL">Alabama</option>
                <option value="AK">Alaska</option>
                <option value="AZ">Arizona</option>
                <option value="AR">Arkansas</option>
                <option value="CA">California</option>
                <option value="CO">Colorado</option>
                <option value="CT">Connecticut</option>
                <option value="DE">Delaware</option>
                <option value="DC">District Of Columbia</option>
                <option value="FL">Florida</option>
                <option value="GA">Georgia</option>
                <option value="HI">Hawaii</option>
                <option value="ID">Idaho</option>
                <option value="IL">Illinois</option>
                <option value="IN">Indiana</option>
                <option value="IA">Iowa</option>
                <option value="KS">Kansas</option>
                <option value="KY">Kentucky</option>
                <option value="LA">Louisiana</option>
                <option value="ME">Maine</option>
                <option value="MD">Maryland</option>
                <option value="MA">Massachusetts</option>
                <option value="MI">Michigan</option>
                <option value="MN">Minnesota</option>
                <option value="MS">Mississippi</option>
                <option value="MO">Missouri</option>
                <option value="MT">Montana</option>
                <option value="NE">Nebraska</option>
                <option value="NV">Nevada</option>
                <option value="NH">New Hampshire</option>
                <option value="NJ">New Jersey</option>
                <option value="NM">New Mexico</option>
                <option value="NY">New York</option>
                <option value="NC">North Carolina</option>
                <option value="ND">North Dakota</option>
                <option value="OH">Ohio</option>
                <option value="OK">Oklahoma</option>
                <option value="OR">Oregon</option>
                <option value="PA">Pennsylvania</option>
                <option value="RI">Rhode Island</option>
                <option value="SC">South Carolina</option>
                <option value="SD">South Dakota</option>
                <option value="TN">Tennessee</option>
                <option value="TX">Texas</option>
                <option value="UT">Utah</option>
                <option value="VT">Vermont</option>
                <option value="VA">Virginia</option>
                <option value="WA">Washington</option>
                <option value="WV">West Virginia</option>
                <option value="WI">Wisconsin</option>
                <option value="WY">Wyoming</option>
                <
                  </select>
        </fieldset>
        <fieldset>
            <legend>Security</legend>
            Password:<input name="txtPassword" type="password" value="<?=$custPassword?>">
            Password Verification:<input name="txtConfirmPassword" type="password" value="<?=$custPassword?>">
        </fieldset>
        <input type="submit" name="btnSubmit" id="btnSubmit" value="Update Account">
        <input type="button" name="btnDelete" id="btnDelete" value="Delete Account" onclick="DeleteAccount('<?=$id?>')">
        <input type="hidden" id="txtID" name="txtID" value="<?=$id?>">
    </form>
</main>
<footer> </br><?php include '../Includes/footer.php'?></footer>
</body>
</html>