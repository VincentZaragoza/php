<?php
    if(empty($_GET["id"]))
    {
        header("location:customerdb.php");
    }

    if(isset($_GET["id"])) {
        $id = $_GET["id"];

        //Database stuff
        include "../Includes/dbconnect.php";
        try {
            $db = new PDO($dsn, $username, $password, $options);
            $sql = $db->prepare("delete from customers where custID = :id");
            $sql->bindValue(":id", $id);
            $sql->execute();
            header("location:customerdb.php");
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    ?>