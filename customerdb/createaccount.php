<?php
$msg = "";
$key =  sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));

if(isset($_POST["btnCreateAccount"]))
{
    if (!empty($_POST["txtFName"]))
    {
        if (!empty($_POST["txtLName"]))
        {
            if (!empty($_POST["txtPhoneNum"]))
            {
                if (!empty($_POST["txtEmail"]))
                {
                    if (!empty($_POST["txtAddress"]))
                    {
                        if (!empty($_POST["txtCity"]))
                        {
                            if (!empty($_POST["txtZipCode"]))
                            {
                                if (!empty($_POST["txtState"]))
                                {
                                    if( $_POST["txtState"]!= 'NA')
                                    {
                                        if(!empty($_POST["txtPassword"]))
                                        {
                                            if(!empty($_POST["txtConfirmPassword"]))
                                            {
                                                if($state != 'NA')
                                                {
                                                    if($_POST["txtPassword"] == $_POST["txtConfirmPassword"])
                                                    {
                                                        $fName = $_POST["txtFName"];
                                                        $lName = $_POST["txtLName"];
                                                        $phoneNum = $_POST["txtPhoneNum"];
                                                        $email = $_POST["txtEmail"];
                                                        $address = $_POST["txtAddress"];
                                                        $state = $_POST["txtState"];
                                                        $city = $_POST["txtCity"];
                                                        $zipCode = $_POST["txtZipCode"];
                                                        $password = $_POST["txtPassword"];

                                                        include "../Includes/dbconnect.php";
                                                        try{
                                                            $db = new PDO($dsn, $username, $password, $options);
                                                            $sql = $db->prepare("insert into customers (custFirstName, custLastName,custAddress,
                                                            custCity,custState,custZip,custPhone,custEmail,custPassword,custKey) VALUE(:FirstName,:LastName,:Address,
                                                            :City,:State,:Zip,:PhoneNumber,:Email,:Password,:Key )");
                                                            $sql->bindValue(":FirstName", $fName);
                                                            $sql->bindValue(":LastName", $lName);
                                                            $sql->bindValue(":Address", $address);
                                                            $sql->bindValue(":City", $city);
                                                            $sql->bindValue(":State", $state);
                                                            $sql->bindValue(":Zip", $zipCode);
                                                            $sql->bindValue(":PhoneNumber", $phoneNum);
                                                            $sql->bindValue(":Email", $email);
                                                            $sql->bindValue(":Password", md5($password . $key));
                                                            $sql->bindValue(":Key",$key);
                                                            $sql->execute();
                                                            header("location:customerdb.php");
                                                        }catch (PDOException $e)
                                                        {
                                                            echo $e->getMessage();
                                                        }
                                                    }else{$msg ="Passwords Didn't Match";}
                                                }else {$msg = "No State";}
                                            }else {$msg = "Verify Password";}
                                        }else {$msg = "No Password";}
                                    }else {$msg = "No State";}
                                }else{$msg = "No State";}
                            }else{$msg = "No Zip Code";}
                        }else{$msg = "No City";}
                    }else{$msg = "No Address";}
                }else{$msg = "No Email";}
            }else{$msg = "No Phone Number";}
        }else{$msg = "No Last Name";}
    }else{$msg = "No First Name";}
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Create Account</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css"/>
    <link rel="stylesheet" type="text/css" href="../css/create_account.css"/>
</head>
<body>
<header><?php include '../Includes/header.php'?></header>
<nav> <?php include '../Includes/nav.php'?> </nav>
<main>

    <h3><?=$msg?></h3>

    <form method="post">
        <fieldset>
            <legend>Customer</legend>
            <label>First Name:</label><input name="txtFName" type="text">
            <label>Last Name:</label> <input name="txtLName" type="text">
            <label>Phone Number:</label> <input name="txtPhoneNum" type="tel" placeholder="(###)###-####">
            <label>Email:</label> <input name="txtEmail" type="email">
        </fieldset>
        <fieldset>
            <legend>Address</legend>
            <label>Address:</label> <input name="txtAddress" type="text"><br>
            <label>City:</label> <input name="txtCity" type="text"><br>
            <label>Zip Code:</label> <input name="txtZipCode" type="text" placeholder="5 Digit Zip Code"><br>
            <label>State:</label> <select name="txtState">
                <option value="NA">Select State</option>
                <option value="AL">Alabama</option>
                <option value="AK">Alaska</option>
                <option value="AZ">Arizona</option>
                <option value="AR">Arkansas</option>
                <option value="CA">California</option>
                <option value="CO">Colorado</option>
                <option value="CT">Connecticut</option>
                <option value="DE">Delaware</option>
                <option value="DC">District Of Columbia</option>
                <option value="FL">Florida</option>
                <option value="GA">Georgia</option>
                <option value="HI">Hawaii</option>
                <option value="ID">Idaho</option>
                <option value="IL">Illinois</option>
                <option value="IN">Indiana</option>
                <option value="IA">Iowa</option>
                <option value="KS">Kansas</option>
                <option value="KY">Kentucky</option>
                <option value="LA">Louisiana</option>
                <option value="ME">Maine</option>
                <option value="MD">Maryland</option>
                <option value="MA">Massachusetts</option>
                <option value="MI">Michigan</option>
                <option value="MN">Minnesota</option>
                <option value="MS">Mississippi</option>
                <option value="MO">Missouri</option>
                <option value="MT">Montana</option>
                <option value="NE">Nebraska</option>
                <option value="NV">Nevada</option>
                <option value="NH">New Hampshire</option>
                <option value="NJ">New Jersey</option>
                <option value="NM">New Mexico</option>
                <option value="NY">New York</option>
                <option value="NC">North Carolina</option>
                <option value="ND">North Dakota</option>
                <option value="OH">Ohio</option>
                <option value="OK">Oklahoma</option>
                <option value="OR">Oregon</option>
                <option value="PA">Pennsylvania</option>
                <option value="RI">Rhode Island</option>
                <option value="SC">South Carolina</option>
                <option value="SD">South Dakota</option>
                <option value="TN">Tennessee</option>
                <option value="TX">Texas</option>
                <option value="UT">Utah</option>
                <option value="VT">Vermont</option>
                <option value="VA">Virginia</option>
                <option value="WA">Washington</option>
                <option value="WV">West Virginia</option>
                <option value="WI">Wisconsin</option>
                <option value="WY">Wyoming</option>
                <
                  </select>
        </fieldset>
        <fieldset>
            <legend>Security</legend>
            Password:<input name="txtPassword" type="password">
            Password Verification:<input name="txtConfirmPassword" type="password">
        </fieldset><br>
        <input name="btnCreateAccount" type="submit" value="Create Account">
        <input name="btnReset" type="submit" value="Reset">
    </form>
    <input name="txtID" id="txtID" type="hidden" value="<?=$id?>">
</main>
<footer> <br><?php include '../Includes/footer.php'?></footer>
</body>
</html>