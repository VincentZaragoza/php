<?php
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

/**
 * Put = Update
 * Post = Insert
 * Get = select
 * Delete = Delete
 */

$app = new \Slim\Slim();

#$app->get("/helloworld","getHello");
#$app->get("/showmember/:MemberName","showmember");
#$app->post("/showmember/:MemberName","addmember");
#$app->post("/addjson","addjson");
$app->get("/races","getRaces");
$app->get("/runners/:race_id","getRunners");
$app->post("/add_runner","add_Runner");
$app->delete("/delete_runner","delete_Runner");
$app->put("/update_runner","update_Runner");
$app->run();



function showmember($memberName){
    echo "Member Name: $memberName";
}
function addjson(){
    $request = \Slim\Slim::getInstance()->request();
    $post_Json = json_decode($request->getBody(),true);

    echo $post_Json["fname"];

}
function addmember($memberName){
    echo "Added Member Name: $memberName";
}
function getHello(){
    echo "Hello World!";
}

function getRaces(){

    include "../../Includes/dbconnect.php";
    try{
        $db = new PDO($dsn,$username,$password,$options);
        $sql = $db->prepare("Select * from race");
        $sql->execute();
        $results = $sql->fetchAll();
        echo'{"Races":'. json_encode($results ).'}';
        $results = null;
        $db = null;

    }catch(PDOException $e){
        $error = $e->getMessage();
        echo'{"Errors":'. json_encode($error ).'}';
    }
}


function getRunners($race_id){

    include "../../Includes/dbconnect.php";
    try{
        $db = new PDO($dsn,$username,$password,$options);
        $sql = $db->prepare("SELECT
                                       memberLogin.memberName,
                                       memberLogin.memberEmail,
                                       race.raceName
                                       FROM
                                       race
                                       INNER JOIN member_race ON member_race.raceID = race.raceID
                                       INNER JOIN memberLogin ON memberLogin.memberID = member_race.memberID
                                       WHERE
                                       member_race.roleID = 3 AND
                                       member_race.raceID = :raceID");
        $sql->bindValue(":raceID",$race_id);
        $sql->execute();
        $results = $sql->fetchAll();
        echo'{"Races":'. json_encode($results ).'}';
        $results = null;
        $db = null;

    }catch(PDOException $e){
        $error = $e->getMessage();
        echo'{"Errors":'. json_encode($error ).'}';
    }
}

function add_Runner(){
    $request = \Slim\Slim::getInstance()->request();
    $post_Json = json_decode($request->getBody(),true);
    #echo $post_Json["test"];

    include "../../Includes/dbconnect.php";
    try{
        $db = new PDO($dsn,$username,$password,$options);
        $sql = $db->prepare("SELECT
                                        memberLogin.memberKey
                                        FROM
                                        memberLogin
                                        INNER JOIN member_race ON memberLogin.memberID = member_race.memberID
                                        WHERE
                                        member_race.roleID = 2 AND                        
                                        member_race.raceID = :raceID AND 
                                        memberLogin.memberKey = :key");
        $sql->bindValue(":raceID",$post_Json["raceID"]);
        $sql->bindValue(":key",$post_Json["key"]);
        $sql->execute();
        $results = $sql->fetch();
        if($results==null){
            echo "Bad key";
        }else{
            $sql = $db->prepare("insert into member_race(memberID,raceID,roleID) values(:memberID,:raceID,3)");
            $sql->bindValue(":memberID",$post_Json["memberID"]);
            $sql->bindValue(":raceID",$post_Json["raceID"]);
            $sql->execute();
        }

        $results = null;
        $db = null;

    }catch(PDOException $e){
        $error = $e->getMessage();
        echo'{"Errors":'. json_encode($error ).'}';
    }


}

function delete_Runner(){
    $request = \Slim\Slim::getInstance()->request();
    $post_Json = json_decode($request->getBody(),true);
    #echo $post_Json["test"];

    include "../../Includes/dbconnect.php";
    try{
        $db = new PDO($dsn,$username,$password,$options);
        $sql = $db->prepare("SELECT
                                        memberLogin.memberKey
                                        FROM
                                        memberLogin
                                        INNER JOIN member_race ON memberLogin.memberID = member_race.memberID
                                        WHERE
                                        member_race.roleID = 2 AND                        
                                        member_race.raceID = :raceID AND 
                                        memberLogin.memberKey = :key");
        $sql->bindValue(":raceID",$post_Json["raceID"]);
        $sql->bindValue(":key",$post_Json["key"]);
        $sql->execute();
        $results = $sql->fetch();
        if($results==null){
            echo "Bad key";
        }else{
            $sql = $db->prepare("delete from member_race WHERE memberID =:memberID and raceID = :raceID");
            $sql->bindValue(":memberID",$post_Json["memberID"]);
            $sql->bindValue(":raceID",$post_Json["raceID"]);
            $sql->execute();
        }

        $results = null;
        $db = null;

    }catch(PDOException $e){
        $error = $e->getMessage();
        echo'{"Errors":'. json_encode($error ).'}';
    }
}

function update_Runner(){
    $request = \Slim\Slim::getInstance()->request();
    $post_Json = json_decode($request->getBody(),true);
    #echo $post_Json["test"];

    include "../../Includes/dbconnect.php";
    try{
        $db = new PDO($dsn,$username,$password,$options);
        $sql = $db->prepare("SELECT
                                        memberLogin.memberKey
                                        FROM
                                        memberLogin
                                        INNER JOIN member_race ON memberLogin.memberID = member_race.memberID
                                        WHERE
                                        member_race.roleID = 2 AND                        
                                        member_race.raceID = :raceID AND 
                                        memberLogin.memberKey = :key");
        $sql->bindValue(":raceID",$post_Json["raceID"]);
        $sql->bindValue(":key",$post_Json["key"]);
        $sql->execute();
        $results = $sql->fetch();
        if($results==null){
            echo "Bad key";
        }else{
            $sql = $db->prepare("update member_race set roleID=:roleID WHERE memberID =:memberID and raceID=:raceID");
            $sql->bindValue(":memberID",$post_Json["memberID"]);
            $sql->bindValue(":raceID",$post_Json["raceID"]);
            $sql->bindValue(":roleID",$post_Json["roleID"]);
            $sql->execute();
        }

        $results = null;
        $db = null;

    }catch(PDOException $e){
        $error = $e->getMessage();
        echo'{"Errors":'. json_encode($error ).'}';
    }
}

